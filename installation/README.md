# ODL TPCE VM installation on OpenLab

Jump into the OpenLab server

```ssh my_config```

Source to get your credentials

```source openstack_openrc```

Test that is ok

```openstack server list```

Get Heat template from onap-mdons repo

```git clone https://gitlab.com/Orange-OpenSource/lfn/onap/onap-mdons.git```

Go to the repo

```cd onap-mdons\installation```

Adapt the file to get your environment (eg public key)

```
  vnf_name: odltpce
  vnf_id: vnf_odltpce
  vf_module_id: vf_odltpce
  odltpce_pub_key: #your pub key
  odltpce_image_name: # image eg debian-9
  odltpce_flavor_name: #flavor eg  m1.large
  odltpce_name_0: odltpce_0
  admin_net_name: # network name, eg admin
  public_net_name: # external network for gloating I, eg ext-net
  port_ssh: 22
  port_odltpce: € specific port to be opened, eg 80
  pce_user_pub_keys:# list of public keys comma separated foruser tpce
```
## Get OpenStack configuration

* Image list: ```openstack image list```
* Flavor list ```openstack flavor list```

## Launch the stack and test it

Test the stack: eg test the values available in the OpenLab environemnt

```openstack stack create -t base_odltpce.yaml -e base_odltpce.env odltpce --parameter password_odltpce=xxxxxx --dry-run```

Launch the Stack

```openstack stack create -t base_odltpce.yaml -e base_odltpce.env --parameter password_odltpce=xxxxxx odltpce```

Check the stack status

```openstack stack show odltpce | grep status```

Check the resources created by the stack
```openstack stack resource list odltpce```

Get output (eg floating IP)
```openstack stack output show odltpce odltpce_public_ip | grep output_value | awk '{print $4}```

Test installation by watching logs

```openstack console log show odltpce```
